import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';


@Injectable()
export class SimpleService {
  constructor(
      private http: HttpClient
  ){}

  /**
   * Dont do this...It is mearly demonstrating a promise to whatever calls it.
   */
  getSWCharacter(param): Promise<Object> {
    return new Promise((resolve, reject) => {
      let sub = this.http
      .get('https://swapi.co/api/people/' + param)
      .subscribe(data => {

        // unsubscribe from subscription
        sub.unsubscribe();
        resolve(data);
      });
    });
  }

  /**
   * Return an Observable that will tick every 4 seconds
   */
  getObservable(): Observable<number> {

    // return and create a new observable
    // The Observalbe takes a function as a parameter
    // the function takes the observer as the parameter and is utilized in the function
    return new Observable(observer => {
      let value = 0;

      // interval will fire 100 times
      let interval = setInterval(() => {
        value++;

        // if value is greater than 100, stop the interval, complete the subscription
        if(value > 100) {
          clearInterval(interval);
          observer.complete();
        } else {
          observer.next(value);
        }
      }, 5);
    });
  }

  getChar(param): Observable<Object> {
    return this.http
      .get('https://swapi.co/api/people/' + param);
  }
}