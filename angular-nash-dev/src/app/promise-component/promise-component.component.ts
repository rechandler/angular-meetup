import { Component, OnInit } from '@angular/core';
import { SimpleService } from '../services/simple-service.service';

import { PromiseService } from './promise-component.service';

@Component({
    selector: 'app-promise-component',
    templateUrl: './promise-component.component.html',
    styleUrls: ['./promise-component.component.css'],
    providers: [PromiseService]
})
export class PromiseComponentComponent implements OnInit {

    private data: any;
    private thingsHappening: number = null;
    private found: Boolean = false;
    private code: string;

    // Data placeholder for promise chain example
    private chainData1: String;
    private chainData2: String;
    private chainData3: String;
    private promiseInfo: Object;
    private promiseChainInfo: Object;

    constructor(
        private simpleService: SimpleService,
        private promiseService: PromiseService
    ) { }

    ngOnInit() {
        this.code = "console.log('this is a test');"
        this.promiseInfo = this.promiseService.promiseInfo;
        this.promiseChainInfo = this.promiseService.promiseChainInfo;
    }

    /**
     * Returns a promise of type boolean
     */
    simplePromise(): Promise<Boolean> {

        // return a new instance of Promise
        return new Promise((resolve, reject) => {
            setTimeout(() => {

                // Wait 5 seconds then resolve the promise as true
                resolve(true)
            }, 5000)
        });
    };

    promiseChain(): void {

        // Go ahead and clear out entries
        this.chainData1 = this.chainData2 = this.chainData3 = null;

        this.simpleService.getChar(1)
            .toPromise()
            .then(result => {
                // set some data for the  ui
                this.chainData1 = result['name'];

                // Remember to call toPromise because getChar actually returns 
                //  an observable
                return this.simpleService.getChar(2)
                    .toPromise();
            })
            .then(result => {
                // set second data for the ui
                this.chainData2 = result['name'];  
                
                // Remember to call toPromise because getChar actually returns 
                //  an observable
                return this.simpleService.getChar(3)
                    .toPromise();
            })
            .then(result => {
                // set third object 
                this.chainData3 = result['name'];                
            })
            .catch(error => console.error(error));
    }

    /**
     * Makes a call to an external API
     */
    starWarsGet(): void {

        //Set found to false so each click will start fresh
        this.found = false;
        this.data = null;

        // Call the service to fetch some data
        this.simpleService.getSWCharacter(1)
        .then(data => {

            // mark found true and set data
            this.found = true;
            this.data = data;
        })
        .catch(err => console.error(err));

        // By using a Promise, we can continue running code while waiting for 
        //  a response from the previously called functions
        this.thingsHappening = 0;
        this.checkIfFound();
    }

    /**
     * Checks if data exists. If it does not, it calls itself
     */
    checkIfFound(){

        // If not found, wait 5 millis
        if(!this.found){
        setTimeout(() => {

            // increase count and recursively call function
            this.thingsHappening += 1;
            this.checkIfFound();
        }, 5);
        }
    }

}
