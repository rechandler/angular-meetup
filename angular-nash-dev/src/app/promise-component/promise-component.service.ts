import { Injectable } from '@angular/core';

@Injectable()
export class PromiseService {
    constructor(){}

    public promiseInfo: Array<Object> = [
        {
            heading: "Promises have been around a few years but were added natively in ES6/ES2015"
        },
        {
            heading: "There are three main states of a promise",
            subPoints: [
                "Pending",
                "Resolved",
                "Rejected"
            ]
        },
        {
            heading: "There are 2 basic functions",
            subPoints: [
                ".then()",
                ".catch()"
            ]
        }
    ];

    public promiseChainInfo: Array<Object> = [
        {
            heading: ".then() creates a new promise and adds it to the \"Promise Chain\"",
            subPoints: [
                "The initial returned promise will not resolve until the last promise in the chain does"
            ]
        },
        {
            heading: "Always returns a promise",
            subPoints: [
                "Even if you don’t actually return anything"
            ]
        },
        {
            heading: "Can chain as many as you want"
        }
    ]
}