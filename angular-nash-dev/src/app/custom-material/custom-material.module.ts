import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {
  MatCardModule,
  MatIconModule,
  MatMenuModule,
  MatInputModule,
  MatTableModule,
  MatButtonModule,
  MatRippleModule,
  MatSidenavModule,
  MatStepperModule,
  MatToolbarModule,
  MatGridListModule,
  MatSnackBarModule,
  MatFormFieldModule,
  MatExpansionModule,
  MatTabsModule
} from '@angular/material'

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  imports: [
    BrowserAnimationsModule,
    CommonModule,
    MatCardModule,
    MatIconModule,
    MatMenuModule,
    MatInputModule,
    MatTableModule,
    MatButtonModule,
    MatRippleModule,
    MatSidenavModule,
    MatStepperModule,
    MatToolbarModule,
    MatGridListModule,
    MatSnackBarModule,
    MatFormFieldModule,
    MatExpansionModule,
    MatTabsModule
  ],
  exports: [
    MatCardModule,
    MatIconModule,
    MatMenuModule,
    MatInputModule,
    MatTableModule,
    MatButtonModule,
    MatRippleModule,
    MatSidenavModule,
    MatStepperModule,
    MatToolbarModule,
    MatGridListModule,
    MatSnackBarModule,
    MatFormFieldModule,
    MatExpansionModule,
    MatTabsModule
  ],
  declarations: []
})
export class CustomMaterialModule { }
