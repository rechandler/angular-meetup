import { Injectable } from '@angular/core';

@Injectable()
export class ObservableService {
    constructor(){}

    public observableData: Array<Object> = [
        {
            heading: "This the most basic building block of RxJS.",
            subPoints: [
                "A representation of any set of values over any amount of time."
            ]
        },
        {
            heading: "Observables are lazy Push collections of multiple values",
            subPoints: [
                "A Promise is a lazy push of a single value."
            ]
        },
        {
            heading: "Observables are just functions with special characteristics",
            subPoints: [
                "The observable takes an observer (an object with 'next', 'error', and 'complete')",
                "It returns a cancellation method"
            ]
        }
    ];
    
    public observerData: Array<Object> = [
        {
            heading: "an Observer subscribes to an Observable",
            subPoints: [
                "Then that observer reacts to whatever item or sequence of items the Observable emits"
            ]
        },
        {
            heading: "When an observable produces values, it notifys the observer by calling the 'next' method",
            subPoints: [
                "Calling .next(parameter) will emit the value of 'parameter' to all of its observers"
            ]
        }
    ]

    public subjectInfo: Array<Object> = [
        {
            heading: "A Subject is a collection of Observables",
            subPoints: [
                "Subjects are the only way of multicasting a value or event to multiple Observers"
            ]
        },
        {
            heading: "Subjects inherit from the Observable class",
            subPoints: [
                
            ]
        }
    ]
}