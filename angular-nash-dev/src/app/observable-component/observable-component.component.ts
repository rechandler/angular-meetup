import { Component, OnInit } from '@angular/core';
import { SimpleService } from '../services/simple-service.service';
import { Observable, Observer, Subject, BehaviorSubject } from 'rxjs';

import { ObservableService } from './observable-component.service';

@Component({
    selector: 'app-observable-component',
    templateUrl: './observable-component.component.html',
    styleUrls: ['./observable-component.component.css'],
    providers: [ObservableService]
})
export class ObservableComponentComponent implements OnInit {

data: any;
observable: Observable<number>;
observer: Observer<number>;
subject: Subject<number>;
bhSubject: BehaviorSubject<number>;

bhValues: Array<String> = [];
subjectData: Array<String> = [];

observableData: Array<Object>;
observerData: Array<Object>;
subjectInfo: Array<Object>;

constructor(
    private simpleService: SimpleService,
    private observableService: ObservableService
    ) { }

    ngOnInit() {
        // Initialize the value of the behavior Subject to 1
        this.bhSubject = new BehaviorSubject<number>(100);

        // subject has no initial value
        this.subject = new Subject<number>();

        this.observableData = this.observableService.observableData;
        this.observerData = this.observableService.observerData;
        this.subjectInfo = this.observableService.subjectInfo;
    }

    /**
     * Kick of an observable sequence
     */
    observableKickoff(): void {

    // Subscribe takes an object with 3 parameters.
    // This object is the observer
    let subscription = this.simpleService.getObservable()
        .subscribe({
            next: data => this.data = data,              // Handle the data
            error: err => console.error(err),            // Log Errors
            complete: () => {
                console.log('Subscription Completed')    // Handle the subscription complete
                subscription.unsubscribe();              // Unsubscribe from the observable
            }
        })
    }

    /**
     * Kick off the behavior Subject
     */
    bhKickoff(): void {
        this.bhValues = [];

        // Set up the first subscriber
        let sub1 = this.bhSubject.subscribe({
            next: value => this.bhValues.push("Behavior Subject A recieved Value: " + value )
        });

        // For loop will tick through values
        for(let i = 0; i < 3; i++){
            this.bhSubject.next(i * 2);
        }

        // set up second subscriber to same subject
        let sub2 = this.bhSubject.subscribe({
            next: value => this.bhValues.push("Behavior Subject B recieved Value: " + value )
        });

        // for loop will tick values to both
        for(let i = 3; i < 6; i++){
            this.bhSubject.next(i * 2);
        }

        // Unsubscribe from the subject
        // If we don't unsubscribe, we will continue to add subscriptions 
        //  to the subject with each button click
        sub1.unsubscribe();
        sub2.unsubscribe();
    } 

    subjectKickoff(): void {
        let observe1, observe2;

        // Will emit an int every second and only take first 5 values
        let tick$ = Observable.interval(1000).take(5);
        
        // set up observabale 1;
        let obs1 = new Observable(observer => {
            // save the observer
            //  we will add it to the subject
            observe1 = observer;
        }).subscribe(
            data => {
                // Push this data into Subject data to be viewed in ui
                this.subjectData.push('Observable 1 data: ' + data)
            }
        );

        // set up observable 2
        let obs2 = new Observable(observer => {
            
            //save the obsever
            // we'll add it to the subject            
            observe2 = observer;
        }).subscribe(
            data => {
                // push this data into Subject data to be viewed in ui
                this.subjectData.push('Observable 2 data: ' + data)
            }
        );

        // Add the observers to the subject.
        this.subject.subscribe(observe1);
        this.subject.subscribe(observe2);

        // every tick, the int emitted from tick$ will be piped to the subject
        //  every observer in the subject will get the value
        tick$.subscribe(this.subject);  

    }

    /**
     * Handle the observable
     * @param data - Data to be set
     */
    handler(data): void {
        this.data = data;
    }

    /**
     * Error Handler
     * @param err - Error Message
     */
    error(err): void {
        console.error(err);
    }

    /**
     * Copmlete function to run
     */
    complete(): void {
        console.log('Subscription Completed');
    }

}
