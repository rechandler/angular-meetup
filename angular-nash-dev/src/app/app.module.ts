import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { PromiseComponentComponent } from './promise-component/promise-component.component';
import { CustomMaterialModule } from './custom-material/custom-material.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { HttpClientModule } from '@angular/common/http';

import { SimpleService } from './services/simple-service.service';
import { ObservableComponentComponent } from './observable-component/observable-component.component';

@NgModule({
  declarations: [
    AppComponent,
    PromiseComponentComponent,
    ObservableComponentComponent
  ],
  imports: [
    BrowserModule,
    CustomMaterialModule,
    FlexLayoutModule,
    HttpClientModule
  ],
  providers: [
    SimpleService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
